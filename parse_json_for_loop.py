def parse_data(data_input, db_schema):
    def remove_non_metric(name):
        not_metric_columns = ['account_', 'adgroup_', 'campaign_', 'ad_']
        if all([not name.startswith(i) for i in not_metric_columns]):
            return name

    metric_columns = list(filter(remove_non_metric, [i[0] for i in db_schema]))
    ads_columns = [i[0] for i in db_schema if i[0].startswith('ad_')]
    adgroups_columns = [i[0] for i in db_schema if i[0].startswith('adgroup_')]
    campaigns_columns = [i[0] for i in db_schema if i[0].startswith('campaign_')]
    account_columns = [i[0] for i in db_schema if i[0].startswith('account_')]

    def metrics(data):
        def inner_metric(row):
            row_parsed = dict()
            for i in metric_columns:
                row_parsed[i] = row[i]
            return row_parsed

        return [inner_metric(i) for i in data]

    def ads(data):
        def inner_ads(row):
            row_parsed = dict()
            for i in ads_columns:
                row_parsed[i] = row[i.replace('ad_', '')]
            return row_parsed

        return [dict(**inner_ads(ad_data), **metric) for ad_data in data
                for metric in metrics(ad_data['metrics']['byDate'])]

    def adgroups(data):
        def inner_adgroups(row):
            row_parsed = dict()
            for i in adgroups_columns:
                row_parsed[i] = row[i.replace('adgroup_', '')]
            return row_parsed

        return [dict(**inner_adgroups(adgroup_data), **ad) for adgroup_data in data
                for ad in ads(adgroup_data['ads'])]

    def campaigns(data):
        def inner_campaigns(row):
            row_parsed = dict()
            for i in campaigns_columns:
                row_parsed[i] = row[i.replace('campaign_', '')]
            return row_parsed

        return [dict(**inner_campaigns(campaign_data), **ad_groups) for campaign_data in data
                for ad_groups in adgroups(campaign_data['adgroups'])]

    def account(data):
        def inner_account(row):
            row_parsed = dict()
            for i in account_columns:
                row_parsed[i] = row[i.replace('account_', '')]
            return row_parsed

        return [dict(**inner_account(account_data), **i) for account_data in data
                for i in campaigns(account_data['campaigns'])]

    return account(data_input)
